package com.htp.single;

/**
 * Created by Владимир on 12.12.2016.
 */
public class Singleton {
    private Singleton() {
        System.out.println("created");
    }

    private final static Singleton INSTANCE;
    static {
        INSTANCE = new Singleton();
    }

    public static Singleton getInstance(){
        return INSTANCE;
    }
}
