package com.htp;

import com.htp.single.Singleton;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Integer i = new Integer(23);
        Long l = new Long(i);
    }
}



class Singletone {
    private Singletone() {
        System.out.println("created");
    }
    private static class Hild{
        private final static Singletone INSTANCE = new Singletone();
    }


    public static Singletone getInstance() {

        return Hild.INSTANCE;
    }

    public static void f() {
        System.out.println("F method");
    }
}