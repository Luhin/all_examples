package com.ex;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Hello world!
 */
public class Main {
    public static void main(String[] args) {
        Num n = new Num();
        ArrayList<Num> list = new ArrayList<Num>(Arrays.asList(
                new Num(), new Num(), new Num(), new Num(), new Num(), new Num()));
        list
                .stream()
                .map(t -> {
                    t.setI(t.getI() + 1);
                    return t;
                })
                .filter(t -> t.getI() < 50)
                .forEach(System.out::println);
    }
}

class Num {
    private int i;

    public Num() {
        Random random = new Random();
        i = random.nextInt(100);
    }

    public void f(int i) {
        //to do
    }

    public long d(Number n) {
        Long l = (Long)n;
        return l;
    }

    public void f(long s) {
        //to do
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}

