package com.ex;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

/**
 * Created by Владимир on 04.01.2017.
 */
public class Concurrency {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        ExecutorService executorService = Executors.newFixedThreadPool(1000);
        Runnable r = () -> {
            executorService.submit(() -> {
                AtomicLong l = counter.getCount();
                l.getAndAdd(1);
            });
        };

        Stream.iterate(0, i -> i++).limit(1000).forEach(i -> r.run());
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.HOURS);
        System.out.println(counter.getCount());
    }


}

class Counter {
    private AtomicLong count = new AtomicLong(0);

    public AtomicLong getCount() {
        return count;
    }
}