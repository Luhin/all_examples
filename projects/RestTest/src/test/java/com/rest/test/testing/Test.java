package com.rest.test.testing;

import com.rest.test.configuration.AppInitializer;
import com.rest.test.configuration.Configuration;
import com.rest.test.entity.User;
import com.rest.test.service.ServicePlusDAO;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Владимир on 28.12.2016.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Configuration.class, AppInitializer.class})
public class Test {
    @Inject
    private ServicePlusDAO servicePlusDAO;
    @Inject
    private ApplicationContext context;

    @org.junit.Test
    public void saveTEst() {
        User user = new User();
        user.setName("Tanya");
        user.setAge(34);
       // servicePlusDAO.save(user);
        System.out.println(user.getId());
    }
}
