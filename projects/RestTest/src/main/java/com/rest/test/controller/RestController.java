package com.rest.test.controller;


import com.rest.test.entity.User;
import com.rest.test.service.ServicePlusDAO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Владимир on 28.12.2016.
 */

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Inject
    private ServicePlusDAO service;

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> allUsers() {
        List<User> list = service.getAll();
        if (list.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable long id) {
        System.out.println("Truing to find user with id " + id + "==============");
        User user = service.get(id);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user, UriComponentsBuilder uBuilder) {
        System.out.println("Creating user " + user.getName() + "==============");
        //is exists method
        service.save(user);
        //HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(uBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<User>(user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable long id, @RequestBody User user) {
        System.out.println("Updating user " + user.getName() + "==============");
        User persist = service.get(id);
        if (persist == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        persist.setAge(user.getAge());
        persist.setName(user.getName());
        service.update(persist);

        return new ResponseEntity<User>(persist, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable long id) {
        System.out.println("delete user with id " + id + "===========");
        User user = service.get(id);
        if (user == null) {
            System.out.println("No user with id " + id);
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        service.remove(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
