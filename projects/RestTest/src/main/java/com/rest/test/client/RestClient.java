package com.rest.test.client;

import com.rest.test.entity.User;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Владимир on 28.12.2016.
 */
public class RestClient {
    private static final String REST_SERVICE_URI = "http://localhost:8088/";

    public static void getAllUsers() {
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> userMAp = restTemplate
                .getForObject(REST_SERVICE_URI + "/user/", List.class);
        if (!userMAp.isEmpty()) {
            for (LinkedHashMap<String, Object> map : userMAp) {
                System.out.println(map.get("name") + "  " + map.get("age"));

            }
        } else {
            System.out.println("No users!");
        }
    }

    public static void getUser(Long id) {
        System.out.println("get user by ID");
        RestTemplate restTemplate = new RestTemplate();
        User user = restTemplate.getForObject(REST_SERVICE_URI + "/user/1", User.class);
        System.out.println(user.getName() + " Name ============");
    }

    public static void createUser() {
        System.out.println("Create user");
        RestTemplate restTemplate = new RestTemplate();
        User user = new User();
        user.setName("Kirill");
        user.setAge(20);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI + "/user/", user, User.class);
        System.out.println("Created , Location = " + uri.toASCIIString() + "=========");
    }

    public static void update() {
        System.out.println("update user");
        RestTemplate restTemplate = new RestTemplate();
        User user = new User();
        user.setName("Riba");
        user.setAge(21);
        restTemplate.put(REST_SERVICE_URI + "/user/8", user);

    }

    public static void deleteUser() {
        System.out.println("DElete user");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI + "/user/7");
    }

    public static void main(String[] args) {
        deleteUser();
        getAllUsers();
    }
}
