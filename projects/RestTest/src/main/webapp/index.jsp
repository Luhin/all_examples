<html>
<body>
<script src="/res/jquery-3.1.1.js"></script>
<script src="/res/my.js"></script>
<h2>Hello World!</h2>
<h2>Get By ID</h2>
<form id="byId">
    <label for="personId">ID : </label><input name="id" id="personId" value="0" type="number"/>
    <input type="submit" value="Get Person By ID"/> <br/><br/>
    <div id="personIdResponse"></div>
</form>


<h2>Submit new Person</h2>
<form id="newPersonForm">
    <label for="nameInput">Name: </label>
    <input type="text" name="name" id="nameInput" />
    <br/>

    <label for="ageInput">Age: </label>
    <input type="text" name="age" id="ageInput" />
    <br/>
    <input type="submit" value="Save Person" /><br/><br/>
    <div id="personFormResponse" class="green"> </div>
</form>

<h2>Edit Person</h2>
<form id="edit">
    <label for="editId">Age: </label>
    <input type="number" name="age" id="editId" value="0"/>
    <br/>
    <label for="editnameInput">Name: </label>
    <input type="text" name="name" id="editnameInput" />
    <br/>

    <label for="editageInput">Age: </label>
    <input type="text" name="age" id="editageInput" />
    <br/>
    <input type="submit" value="Edit Person" /><br/><br/>
    <div id="editpersonFormResponse" class="green"> </div>
</form>

<form id="all">
    <input type="submit" value="Get ALL users"/> <br/><br/>
    <div id="allAnswer"></div>
</form>
</body>
</html>
