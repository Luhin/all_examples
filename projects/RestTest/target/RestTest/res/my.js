$(document).ready(function () {

    $('#byId').submit(function (e) {
        var personId = +$('#personId').val();
        $.ajax({
            type: 'GET',
            url: '/user/' + personId,
            contentType: 'text/html; charset=utf-8',
            success: function (user) {
                $('#personIdResponse').text(user.name + ', age ' + user.age);
            }
        });
        e.preventDefault(); // prevent actual form submit
    });

    $('#edit').submit(function (e) {
        var personId = +$('#editId').val();
        var data = {'name': $('#editnameInput').val(), 'age': $('#editageInput').val()};
        $.ajax({
            type: 'PUT',
            url: '/user/' + personId,
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (user) {
                $('#editpersonFormResponse').text(user.name + ', age ' + user.age);
            }
        });
        e.preventDefault(); // prevent actual form submit
    });

    $('#newPersonForm').submit(function (e) {
        var data = {'name': $('#nameInput').val(), 'age': $('#ageInput').val()};
        $.ajax({
            type: 'POST',
            url: '/user/',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (user) {
                $('#personFormResponse').text('new id = ' + user.id);
            }
        });
        e.preventDefault(); // prevent actual form submit and page reload
    });

    $('#all').submit(function (e) {
        $.ajax({
            type: 'GET',
            url: '/user/',
            success: function (response) {
                var answer = '';
                $.each(response, function(index, el) {
                    answer = answer + ' name ' + el.name + ' age ' + el.age;
                });
                $('#allAnswer').text(answer);
            }
        });
        e.preventDefault(); // prevent actual form submit and page reload
    });


});