package com.htp.spring.test;

import com.htp.spring.config.ApplicationContextConfig;
import com.htp.spring.dao.BaseIterface;
import com.htp.spring.dao.PersonDAO;
import com.htp.spring.entity.Person;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 * Created by Владимир on 12.12.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationContextConfig.class})
public class Test {
    @Autowired
    ApplicationContext context;
    @Autowired
    private PersonDAO personDAO;

    @org.junit.Test
    public void ff() {
        Person persogn = personDAO.findByLogin("ooo");
        System.out.println(persogn.getPersonAge());
    }
}
