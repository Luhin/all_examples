package com.htp.spring.dao;

import com.htp.spring.entity.Person;

/**
 * Created by Владимир on 13.12.2016.
 */
public interface BaseIterface {
     Person findByLogin(String login);
}
