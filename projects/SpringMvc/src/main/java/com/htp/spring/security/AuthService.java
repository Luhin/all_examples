package com.htp.spring.security;

import com.htp.spring.entity.Person;
import com.htp.spring.entity.UserRole;
import com.htp.spring.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Владимир on 08.12.2016.
 */
@org.springframework.stereotype.Service("authenticationService")
public class AuthService implements UserDetailsService {
    @Autowired
    private Service service;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        System.out.println("AUTH SERVICE +================================");
        Person person = service.findByLogin(login);
        Set<GrantedAuthority> roles = new HashSet();
        if (login.equals("ooo")) {
            roles.add(new SimpleGrantedAuthority(UserRole.ADMIN.toString()));
        } else {
            roles.add(new SimpleGrantedAuthority(UserRole.CLIENT.toString()));
        }
        UserDetails userDetails = new User(person.getName(), Integer.toString(person.getPersonAge()), roles);
        return userDetails;
    }
}
