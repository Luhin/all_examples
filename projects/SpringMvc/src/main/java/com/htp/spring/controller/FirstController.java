package com.htp.spring.controller;

import com.htp.spring.entity.Person;
import com.htp.spring.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by Владимир on 07.12.2016.
 */
@Controller
@SessionAttributes("person")
public class FirstController {
    @Autowired
    private Service service;
    @RequestMapping(value = "login", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView login(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        System.out.println(request.getHeader("referer"));
        Person person = new Person();
        person.setTest("VOVAAA");
        modelAndView.addObject("pers", person);
        modelAndView.setViewName("user");
        return modelAndView;
    }



    @RequestMapping(value = "client", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView firsttt(HttpServletRequest request, HttpServletResponse response) {
        System.out.println(request.getHeader("referer"));
        String last = request.getHeader("referer").substring(22);
        System.out.println(last);
        return new ModelAndView("redirect:login");
    }

    public static void main(String[] args) {
        System.out.println("GOOD BYE!");
    }
}
