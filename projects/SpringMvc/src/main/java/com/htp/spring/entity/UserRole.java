package com.htp.spring.entity;

/**
 * Created by Владимир on 09.12.2016.
 */
public enum UserRole {
    ADMIN, CLIENT
}
