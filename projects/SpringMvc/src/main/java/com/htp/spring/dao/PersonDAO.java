package com.htp.spring.dao;

import com.htp.spring.entity.Person;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Владимир on 07.12.2016.
 */
@Repository
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = DAOException.class)
public class PersonDAO extends BaseDAO /*implements BaseIterface*/{

    public int save(Person t) throws DAOException {
        try {
            Session session = sessionFactory.getCurrentSession();
            int result = (Integer) session.save(t);
            return result;
        } catch (Throwable e) {
            throw new DAOException(e);
        }
    }

    public Person find(int id) {
        Session session = sessionFactory.getCurrentSession();
        Person t = session.get(Person.class, id);
        return t;

    }
    public Person findByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        Person user = null;
        Criteria criteria = session.createCriteria(Person.class);
        criteria.add(Restrictions.eq("name", login));
        Object result = criteria.uniqueResult();
        if (result != null) {
            user = (Person) result;
        }
        return user;
    }
}
