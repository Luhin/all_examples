package com.htp.spring.main;

import com.htp.spring.entity.Person;
import com.htp.spring.service.Service;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * Created by Владимир on 07.12.2016.
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();

        Person person = context.getBean(Person.class);
        System.out.println(person.getName());

        context.close();
    }
}
