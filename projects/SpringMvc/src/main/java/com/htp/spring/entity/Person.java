package com.htp.spring.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.*;

/**
 * Created by Владимир on 07.12.2016.
 */
@Entity
@Component
@Scope("prototype")
public class Person extends BaseEntity implements InitializingBean, DisposableBean {
    @Transient
    private final static Logger logger = LogManager.getLogger();
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int personAge;
    @Transient
    private String test;
    @Autowired
    @OneToOne(mappedBy = "person", cascade = CascadeType.ALL)
    private Address address;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public void f(){
        System.out.println("f in person");
    }
    @PostConstruct
    public void intiM() {
        logger.info("init method");
    }

    public void init() {
        logger.info("Default init method");
    }
    @PreDestroy
    public void desrr() {
        logger.info("destroy method");
    }

    public int getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Disposble destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("InitializingBean after proposition set (init)");
    }
}
