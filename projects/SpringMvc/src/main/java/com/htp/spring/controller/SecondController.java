package com.htp.spring.controller;

import com.htp.spring.entity.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Владимир on 14.12.2016.
 */
@Controller
@RequestMapping(value = "admin/")
//@SessionAttributes("person")
public class SecondController {
    @RequestMapping(value = "admir", method = RequestMethod.POST)
    public String firstt(@ModelAttribute("pers") Person person) {
        System.out.println(person.getTest() + "+=======================");
        return "admin/admir";
    }
}
