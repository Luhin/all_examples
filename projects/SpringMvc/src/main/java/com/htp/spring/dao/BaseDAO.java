package com.htp.spring.dao;

import org.hibernate.SessionFactory;

import javax.inject.Inject;

/**
 * Created by Владимир on 13.12.2016.
 */
public class BaseDAO {

    protected SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    @Inject
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
