package com.htp.spring.service;

import com.htp.spring.dao.DAOException;
import com.htp.spring.dao.PersonDAO;
import com.htp.spring.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Владимир on 07.12.2016.
 */
@Component
public class Service {
    @Autowired
    private PersonDAO personDAO;

    public PersonDAO getPersonDAO() {
        return personDAO;
    }

    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public void save(Person p) {
        try {
            personDAO.save(p);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
    public Person findByLogin(String login) {
        Person person = personDAO.findByLogin(login);
        return person;
    }
}
