package com.htp.spring.entity;

/**
 * Created by Владимир on 12.12.2016.
 */
public class BaseEntity {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
