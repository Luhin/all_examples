package com.htp.spring.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Владимир on 09.12.2016.
 */
@Service()
public class AuthSuccService implements AuthenticationSuccessHandler{
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        System.out.println("AUTH SUCCESS SERVICE ==========================");
        List<? extends GrantedAuthority> list = (List<? extends GrantedAuthority>) authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : list){
            if(grantedAuthority.getAuthority().equals("admin")){
                redirectStrategy.sendRedirect(httpServletRequest,httpServletResponse, "/last");
                break;
            }
            if(grantedAuthority.getAuthority().equals("client")){
                redirectStrategy.sendRedirect(httpServletRequest,httpServletResponse, "/user");
                break;
            }
        }
    }
}
