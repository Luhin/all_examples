package com.htp.spring.main;

import com.htp.spring.dao.DAOException;
import com.htp.spring.dao.Service;
import com.htp.spring.entity.Person;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Владимир on 05.12.2016.
 */
public class Main {
    public static void main(String[] args) throws DAOException {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

        Person person = new Person();
        person.setName("transction test");
        person.setPersonAge(1222);
        Service service = context.getBean(Service.class);
        service.save(person);
        System.out.println("sdfgh");

        context.close();
    }
}
