package com.htp.spring.dao;

import com.htp.spring.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Владимир on 06.12.2016.
 */

@Repository
public class PersonDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int save(Person t) throws DAOException {
        try {
            Session session = sessionFactory.openSession();
            int result = (Integer) session.save(t);
            int i = 10 / 0;
            return result;
        } catch (Throwable e) {
            throw new RuntimeException();
        }
    }

    public Person find(int id) {
        Session session = sessionFactory.openSession();
        Person t = session.get(Person.class, id);
        return t;

    }
}
