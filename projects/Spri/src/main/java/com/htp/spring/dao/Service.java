package com.htp.spring.dao;

import com.htp.spring.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Владимир on 06.12.2016.
 */
@Component
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Throwable.class)
public class Service {
    @Autowired
    private PersonDAO personDAO;

    public PersonDAO getPersonDAO() {
        return personDAO;
    }

    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public void save(Person p) {
        try {
            personDAO.save(p);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }
}
