package com.htp.spring.entity;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Created by Владимир on 06.12.2016.
 */
@Aspect
@Component
public class Asp {
    @Pointcut("execution(* com.htp.spring.entity.Person.f(..))")
    public void pointcut(){}
    @Before("pointcut()")
    public void bef() {
        System.out.println("before in Asp");
    }
    @After("pointcut()")
    public void aft() {
        System.out.println("after in Asp");
    }
    @Around("pointcut()")
    public void arr(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("aroun first");
        pjp.proceed();
        System.out.println("arounf after");
    }

}
